package grpc

import (
	"gitlab.com/market3723841/sale-service.git/config"
	"gitlab.com/market3723841/sale-service.git/gRPC/client"
	"gitlab.com/market3723841/sale-service.git/gRPC/service"
	sale_service "gitlab.com/market3723841/sale-service.git/genproto"
	"gitlab.com/market3723841/sale-service.git/pkg/logger"
	"gitlab.com/market3723841/sale-service.git/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	sale_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	sale_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(cfg, log, strg, srvc))
	sale_service.RegisterStaffTransactionServiceServer(grpcServer, service.NewStaffTransactionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
